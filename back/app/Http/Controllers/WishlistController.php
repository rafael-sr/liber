<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wishlist;
use App\Models\User;
use App\Models\Book;

class WishlistController extends Controller
{
    // Create
    public function createWishlist(Request $request) {
        $wishlist = new Wishlist;
        $wishlist->createWishlist($request);

        return response()->json(['wishlist' => $wishlist], 200);
    }

    // Read
    public function showWishlists(){
        $wishlists = Wishlist::all();

        return response()->json(['wishlists' => $wishlists], 200);
    }

    public function showWishlist($id){
        $wishlist = Wishlist::find($id);

        return response()->json(['wishlist' => $wishlist], 200);
    }

    // Update
    public function updateWishlist(Request $request, $id) {
        $wishlist = Wishlist::find($id);
        $wishlist->updateWishlist($request);

        return response()->json(['wishlist' => $wishlist], 200);
    }

    // Delete
    public function deleteWishlist($id){
        $wishlist = Wishlist::find($id);
        $wishlist->delete();

        return response()->json(['lista de desejos deletado'], 200);
    }

    // Adds a wishlist into an user
    public function addWishlist($id, $user_id){
        $wishlist = Wishlist::findOrFail($id);
        $user = User::findOrFail($user_id);
        $wishlist->user_id = $user;

        $wishlist->save();

        return response()->json($wishlist);
    }

    // Removes a wishlist into an user
    public function removeWishlist($id, $user_id){
        $wishlist = Wishlist::findOrFail($id);
        $user = User::findOrFail($user_id);
        $wishlist->$user = NULL;

        $wishlist->save();

        return response()->json($wishlist);
    }

    /* WIP - Bounds a wishlist to an user and book
    public function addOnWishlist($id, $user_id, $book_id){
        $wishlist = Wishlist::findOrFail($id);
        $user = User::findOrFail($user_id);
        $book = Book::findOrFail($book_id);
        $wishlist->user_id = $user;
        $wishlist->book_id = $book;

        $wishlist->save();

        return response()->json($wishlist);
    }
    */
}
