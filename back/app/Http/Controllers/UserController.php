<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    // Create
    public function createUser(Request $request) {
        $user = new User;
        $user->createUser($request);

        return response()->json(['user' => $user], 200);
    }

    // Read
    public function showUsers(){
        $users = User::all();

        return response()->json(['users' => $users], 200);
    }

    public function showUser($id){
        $user = User::find($id);

        return response()->json(['user' => $user], 200);
    }

    // Update
    public function updateUser(Request $request, $id) {
        $user = User::find($id);
        $user->updateUser($request);

        return response()->json(['user' => $user], 200);
    }

    // Delete
    public function deleteUser($id){
        $user = User::find($id);
        $user->delete();

        return response()->json(['usuário deletado'], 200);
    }
}
