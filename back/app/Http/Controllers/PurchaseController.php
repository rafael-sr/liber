<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Purchase;

class PurchaseController extends Controller
{
    // Create
    public function createPurchase(Request $request){
        $purchase = new Purchase;
        $purchase->createPurchase($request);

        return response()->json(['purchase' => $purchase], 200);
    }

    // Read
    public function showPurchases(){
        $purchases = Purchase::all();

        return response()->json(['purchases' => $purchases], 200);
    }

    public function showPurchase($id){
        $purchase = Purchase::find($id);

        return response()->json(['purchase' => $purchase], 200);
    }

}
