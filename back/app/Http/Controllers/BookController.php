<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    // Create
    public function createBook(Request $request){
        $book = new Book;
        $book->createBook($request);

        return response()->json(['book' => $book], 200);
    }

    // Read
    public function showBooks(){
        $books = Book::all();

        return response()->json(['books' => $books], 200);
    }

    public function showBook($id){
        $book = Book::find($id);

        return response()->json(['book' => $book], 200);
    }


    // Update
    public function updateBook(Request $request, $id){
        $book = Book::find($id);
        $book->updateBook($request);

        return response()->json(['book' => $book], 200);
    }

    // Delete
    public function deleteBook($id){
        $book = Book::find($id);
        $book->delete();

        return response()->json(['livro deletado'], 200);
    }
}
