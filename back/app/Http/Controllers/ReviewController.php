<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Review;
use App\Models\Book;
use App\Models\User;

class ReviewController extends Controller
{
    // Create
    public function createReview(Request $request){
        $review = new Review;
        $review->createReview($request);

        return response()->json(['review' => $review], 200);
    }

    // Read
    public function showReviews(){
        $reviews = Review::all();

        return response()->json(['reviews' => $reviews], 200);
    }

    public function showReview($id){
        $review = Review::find($id);

        return response()->json(['review' => $review], 200);
    }

    // // WIP - Bounds a review to a book and user.
    // public function addReview($id, $book_id, $user_id){
    //     $review = Review::findOrFail($id);
    //     $book = Book::findOrFail($book_id);
    //     $user = User::findOrFail($user_id);
    //     $review->book_id = $book;
    //     $review->user_id = $user;

    //     $review->save();

    //     return response()->json($review);
    // }
}
