<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // Relationships
    public function reviews(){
        return $this->hasMany('App\Models\Review');
    }

    public function wishlists(){
        return $this->hasMany('App\Models\Wishlist');
    }

    public function purchases(){
        return $this->hasMany('App\Models\Purchase');
    }

    // Create
    public function createUser(Request $request) {
        $this->name = $request->name;
        $this->email = $request->email;
        $this->password = bcrypt($request->password);
        $this->cellphone = $request->cellphone;
        $this->CPF = $request->CPF;
        $this->gender = $request->gender;
        $this->birthdate = $request->birthdate;

        $this->save();
    }

    // Update
    public function updateUser(Request $request) {
        if ($request->name) {
            $this->name = $request->name;
        }
        if ($request->email) {
            $this->email = $request->email;
        }
        if ($request->password) {
            $this->password = $request->password;
        }
        if ($request->cellphone) {
            $this->cellphone = $request->cellphone;
        }
        if ($request->CPF) {
            $this->CPF = $request->CPF;
        }
        if ($request->gender) {
            $this->gender = $request->gender;
        }
        if ($request->birthdate) {
            $this->birthdate = $request->birthdate;
        }

        $this->save();
    }
}
