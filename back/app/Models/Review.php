<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Review extends Model
{
    use HasFactory;

    // Relationships
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function book(){
        return $this->belongsTo('App\Models\Book');
    }

    // Create
    public function createReview(Request $request){
        $this->title = $request->title;
        $this->rating = $request->rating;
        $this->review = $request->review;
        $this->user_id = $request->user_id;
        $this->book_id = $request->book_id;

        $this->save();
    }
}
