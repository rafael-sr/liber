<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Wishlist extends Model
{
    use HasFactory;


    // Relationships
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function book(){
        return $this->hasMany('App\Models\Book');
    }

    // Create
    public function createWishlist(Request $request) {
        $this->user_id = $request->user_id;
        $this->books_id = $request->books_id;

        $this->save();
    }

    // Update
    public function updateWishlist(Request $request) {
        if ($request->books_id) {
            $this->books_id = $request->books_id;
        }

        $this->save();
    }
}
