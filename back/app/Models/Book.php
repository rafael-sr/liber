<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Book extends Model
{
    use HasFactory;

    // Relationships

    public function review(){
        return $this->hasMany('App\Models\Review');
    }

    public function wishlists(){
        return $this->belongsToMany('App\Models\Wishlist');
    }

    public function purchases(){
        return $this->belongsToMany('App\Models\Purchase');
    }

    // Create
    public function createBook(Request $request){
        $this->name = $request->name;
        $this->synopsis = $request->synopsis;
        $this->price = $request->price;
        $this->gender = $request->gender;
        $this->rating = $request->rating;

        $this->save();
    }

    // Update
    public function updateBook(Request $request, $id){
        if($request->name){
            $this->name = $request->name;
        }
        if($request->synopsis){
            $this->synopsis = $request->synopsis;
        }
        if($request->price){
            $this->price = $request->price;
        }
        if($request->gender){
            $this->gender = $request->gender;
        }
        if($request->rating){
            $this->rating = $request->rating;
        }

        $this->save();
    }
}
