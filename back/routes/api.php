<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\PurchaseController;
use App\Http\Controllers\WishlistController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// User
Route::POST('users', [UserController::class, 'createUser']);

Route::GET('users', [UserController::class, 'showUsers']);
Route::GET('users/{id}', [UserController::class, 'showUser']);

Route::PUT('users/{id}', [UserController::class, 'updateUser']);

Route::DELETE('users/{id}', [UserController::class, 'deleteUser']);

// Book
Route::POST('books', [BookController::class, 'createBook']);

Route::GET('books', [BookController::class, 'showBooks']);
Route::GET('books/{id}', [BookController::class, 'showBook']);

Route::PUT('books/{id}', [BookController::class, 'updateBook']);

Route::DELETE('books/{id}', [BookController::class, 'deleteBook']);

// Review
Route::POST('reviews', [ReviewController::class, 'createReview']);

Route::GET('reviews', [ReviewController::class, 'showReviews']);
Route::GET('reviews/{id}', [ReviewController::class, 'showReview']);

/* WIP
Route::PUT('reviews/{id}', [ReviewController::class, 'addReview']);

// Purchase
Route::POST('purchases', [PurchaseController::class, 'createPurchase']);

Route::GET('purchases', [PurchaseController::class, 'showPurchases']);
Route::GET('purchases/{id}', [PurchaseController::class, 'showPurchases']);

// Wishlist
Route::PUT('wishlists/{id}', [WishlistController::class, 'updateWishlist']);

Route::PUT('wishlists', [WishlistController::class, 'addOnWishlist']);

Route::DELETE('wishlists/{id}', [WishlistController::class, 'deleteWishlist']);
*/

Route::POST('wishlists', [WishlistController::class, 'createWishlist']);

Route::GET('wishlists', [WishlistController::class, 'showWishlists']);
Route::GET('wishlists/{id}', [WishlistController::class, 'showWishlist']);

Route::PUT('wishlists/{id}', [WishlistController::class, 'addWishlist']);
Route::PUT('wishlists/{id}', [WishlistController::class, 'removeWishlist']);

