import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

class Banner{
  image: string;
}

class Book{
  id: number;
  name: string;
  price: string;
  gender: string;
  rating: number;
  image: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  @Input() book;
  
  slideOptsBanner = {
    slidesPerView: 1,
    autoplay: true
  };

  slideOptsSlides = {
    slidesPerView: 3,

  };

  public banners: Banner[];
  public choiceBased: Book[];
  public daily: Book[];
  public fiction: Book[];
  public literature: Book[];
  public new: Book[];

  constructor(public router: Router) { }

  ngOnInit() {
    this.banners = [
      {
        image: "./assets/banner/1.png"
      },
      {
        image: "./assets/banner/2.png"
      },
      {
        image: "./assets/banner/3.png"
      },
      {
        image: "./assets/banner/4.png"
      },
    ];
  
    this.choiceBased = [
      {
        id: 1,
        name: "Clean Code: A Handbook of Agile Software Craftsmanship",
        price: "384.97",
        gender: "technology",
        rating: 4.7,
        image: "./assets/choice-based/1.jpg"
      },
      {
        id: 2,
        name: "Naruto Gold - Volume 1",
        price: "39.99",
        gender: "manga",
        rating: 4.4,
        image: "./assets/choice-based/2.jpg"
      },
      {
        id: 3,
        name: "Do Mil ao Milhão. Sem Cortar o Cafezinho.",
        price: "17.89",
        gender: "economy",
        rating: 4.2,
        image: "./assets/choice-based/3.jpg"
      },
      {
        id: 4,
        name: "A revolução dos bichos: Um conto de fadas",
        price: "19.99",
        gender: "fiction",
        rating: 5.0,
        image: "./assets/choice-based/4.jpg"
      },
      {
        id: 5,
        name: "As Aventuras na Netoland com Luccas Neto",
        price: "9.99",
        gender: "manga",
        rating: 0.5,
        image: "./assets/choice-based/5.jpg"
      }
    ];

    this.daily = [
      {
        id: 6,
        name: "A seleção",
        price: "27.28",
        gender: "romance",
        rating: 4.8,
        image: "./assets/daily/1.jpg"
      },
      {
        id: 7,
        name: "Hunter X Hunter - Vol. 1",
        price: "29.99",
        gender: "manga",
        rating: 5.0,
        image: "./assets/daily/2.jpg"
      },
      {
        id: 8,
        name: "O Ladrão de Raios - Volume 1",
        price: "28.89",
        gender: "fiction",
        rating: 5,
        image: "./assets/daily/3.jpg"
      },
      {
        id: 9,
        name: "Introdução à Programação com Python",
        price: "68.67",
        gender: "technology",
        rating: 4.8,
        image: "./assets/daily/4.jpg"
      },
      {
        id: 10,
        name: "Solo Leveling – Volume 02",
        price: "52.90",
        gender: "novel",
        rating: 5,
        image: "./assets/daily/5.jpg"
      }
    ];

    this.fiction = [

      {
        id: 11,
        name: "A Cilada (Cidade das Sombras Livro 2)",
        price: "18.90",
        gender: "fiction",
        rating: 4.3,
        image: "./assets/fiction/1.jpg"
      },
      {
        id: 12,
        name: "A Ilha",
        price: "11.90",
        gender: "fiction",
        rating: 5,
        image: "./assets/fiction/2.jpg"
      },
      {
        id: 13,
        name: "Admirável mundo novo",
        price: "22.89",
        gender: "fiction",
        rating: 5,
        image: "./assets/fiction/3.jpg"
      },
      {
        id: 14,
        name: "Convergente",
        price: "40.90",
        gender: "fiction",
        rating: 4.7,
        image: "./assets/fiction/4.jpg"
      },
      {
        id: 15,
        name: "Imagina-me: 6",
        price: "30.90",
        gender: "fiction",
        rating: 4.2,
        image: "./assets/fiction/5.jpg"
      }
    ];

    this.literature = [

      {
        id: 16,
        name: "Os miseráveis",
        price: "65.90",
        gender: "literature",
        rating: 4.2,
        image: "./assets/literature/1.jpg"
      },
      {
        id: 17,
        name: "A Tempestade",
        price: "35.09",
        gender: "literature",
        rating: 4.4,
        image: "./assets/literature/2.jpg"
      },
      {
        id: 18,
        name: "Odisseia",
        price: "35.83",
        gender: "literature",
        rating: 5,
        image: "./assets/literature/3.jpg"
      },
      {
        id: 19,
        name: "Hamlet",
        price: "27.28",
        gender: "literature",
        rating: 5,
        image: "./assets/literature/4.jpg"
      },
      {
        id: 20,
        name: "A tragédia de Macbeth",
        price: "20.00",
        gender: "literature",
        rating: 4.4,
        image: "./assets/literature/5.jpg"
      }
    ];

    this.new = [
      {
        id: 21,
        name: "A cinco passos de você",
        price: "19.89",
        gender: "romance",
        rating: 4.5,
        image: "./assets/new/1.jpg"
      },
      {
        id: 22,
        name: "A metamorfose",
        price: "23.68",
        gender: "fiction",
        rating: 4.3,
        image: "./assets/new/2.jpg"
      },
      {
        id: 23,
        name: "A rainha vermelha",
        price: "27.99",
        gender: "fiction",
        rating: 4.4,
        image: "./assets/new/3.jpg"
      },
      {
        id: 24,
        name: "A arte da guerra",
        price: "9.90",
        gender: "leadership",
        rating: 4.2,
        image: "./assets/new/4.jpg"
      },
      {
        id: 25,
        name: "Patrulha canina - 365 atividades e desenhos para colorir",
        price: "7.90",
        gender: "children",
        rating: 4.4,
        image: "./assets/new/5.jpg"
      },
    ];
  }

  go(id) {
    this.router.navigate(['/book', {bookId: id}])
  }
}
