import { Component, OnInit, Input } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book-sell',
  templateUrl: './book-sell.component.html',
  styleUrls: ['./book-sell.component.scss'],
})
export class BookSellComponent implements OnInit {
  @Input() book;
  bookId: number;

  constructor(public alertController: AlertController, public toastController: ToastController, private router: ActivatedRoute) { 
    this.bookId = this.router.snapshot.params['bookId'];
  }

  async alerta(){
    const alert = await this.alertController.create({
      header: 'Cupom de desconto ou vale-presente',
      inputs: [
        {
          name: 'coupon',
          type: 'text',
          placeholder: 'Insira o código'
        }
      ],
      buttons: ['Aplicar']
    });

    await alert.present();
  }

  async presentToast(){
    const toast = await this.toastController.create({
      cssClass: 'toast',
      message: 'Item adicionado ao carrinho',
      duration: 2000      
    });

    toast.present();
  }
  
  ngOnInit() {
    console.log(this.bookId);
  }

}
