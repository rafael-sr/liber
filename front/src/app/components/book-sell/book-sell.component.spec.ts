import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookSellComponent } from './book-sell.component';

describe('BookSellComponent', () => {
  let component: BookSellComponent;
  let fixture: ComponentFixture<BookSellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookSellComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookSellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
