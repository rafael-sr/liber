import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

class Book{
  name: string;
  author: string;
  price: string;
  gender: string;
  rating: number;
  image: string;
  description: string;
}

@Component({
  selector: 'app-book',
  templateUrl: './book.page.html',
  styleUrls: ['./book.page.scss'],
})

export class BookPage implements OnInit {
  books: Book[];

  constructor(public alertController: AlertController, public toastController: ToastController) { }


  async alerta(){
    const alert = await this.alertController.create({
      header: 'Cupom de desconto ou vale-presente',
      inputs: [
        {
          name: 'coupon',
          type: 'text',
          placeholder: 'Insira o código'
        }
      ],
      buttons: ['Aplicar']
    });

    await alert.present();
  }

  async presentToast(){
    const toast = await this.toastController.create({
      cssClass: 'toast',
      message: 'Item adicionado ao carrinho',
      duration: 2000      
    });

    toast.present();
  }

  ngOnInit() {
    this.books = [
      {
        name: "Clean Code: A Handbook of Agile Software Craftsmanship",
        author: "Martin Robert C.",
        price: "384.97",
        gender: "technology",
        rating: 4.7,
        image: "./assets/choice-based/1.jpg",
        description: "Even bad code can function. But if code isn’t clean, it can bring a development organization to its knees. Every year, countless hours and significant resources are lost because of poorly written code. But it doesn’t have to be that way."
      },
      // {
      //   name: "Naruto Gold - Volume 1",
      //   price: "39.99",
      //   gender: "manga",
      //   rating: 4.4,
      //   image: "./assets/choice-based/2.jpg"
      // },
      // {
      //   name: "Do Mil ao Milhão. Sem Cortar o Cafezinho.",
      //   price: "17.89",
      //   gender: "economy",
      //   rating: 4.2,
      //   image: "./assets/choice-based/3.jpg"
      // },
      // {
      //   name: "A revolução dos bichos: Um conto de fadas",
      //   price: "19.99",
      //   gender: "fiction",
      //   rating: 5.0,
      //   image: "./assets/choice-based/4.jpg"
      // },
      // {
      //   name: "As Aventuras na Netoland com Luccas Neto",
      //   price: "9.99",
      //   gender: "manga",
      //   rating: 0.5,
      //   image: "./assets/choice-based/5.jpg"
      // },
      // {
      //   name: "A seleção",
      //   price: "27.28",
      //   gender: "romance",
      //   rating: 4.8,
      //   image: "./assets/daily/1.jpg"
      // },
      // {
      //   name: "Hunter X Hunter - Vol. 1",
      //   price: "29.99",
      //   gender: "manga",
      //   rating: 5.0,
      //   image: "./assets/daily/2.jpg"
      // },
      // {
      //   name: "O Ladrão de Raios - Volume 1",
      //   price: "28.89",
      //   gender: "fiction",
      //   rating: 5,
      //   image: "./assets/daily/3.jpg"
      // },
      // {
      //   name: "Introdução à Programação com Python",
      //   price: "68.67",
      //   gender: "technology",
      //   rating: 4.8,
      //   image: "./assets/daily/4.jpg"
      // },
      // {
      //   name: "Solo Leveling – Volume 02",
      //   price: "52.90",
      //   gender: "novel",
      //   rating: 5,
      //   image: "./assets/daily/5.jpg"
      // },
      // {
      //   name: "A Cilada (Cidade das Sombras Livro 2)",
      //   price: "18.90",
      //   gender: "fiction",
      //   rating: 4.3,
      //   image: "./assets/fiction/1.jpg"
      // },
      // {
      //   name: "A Ilha",
      //   price: "11.90",
      //   gender: "fiction",
      //   rating: 5,
      //   image: "./assets/fiction/2.jpg"
      // },
      // {
      //   name: "Admirável mundo novo",
      //   price: "22.89",
      //   gender: "fiction",
      //   rating: 5,
      //   image: "./assets/fiction/3.jpg"
      // },
      // {
      //   name: "Convergente",
      //   price: "40.90",
      //   gender: "fiction",
      //   rating: 4.7,
      //   image: "./assets/fiction/4.jpg"
      // },
      // {
      //   name: "Imagina-me: 6",
      //   price: "30.90",
      //   gender: "fiction",
      //   rating: 4.2,
      //   image: "./assets/fiction/5.jpg"
      // },
      // {
      //   name: "Os miseráveis",
      //   price: "65.90",
      //   gender: "literature",
      //   rating: 4.2,
      //   image: "./assets/literature/1.jpg"
      // },
      // {
      //   name: "A Tempestade",
      //   price: "35.09",
      //   gender: "literature",
      //   rating: 4.4,
      //   image: "./assets/literature/2.jpg"
      // },
      // {
      //   name: "Odisseia",
      //   price: "35.83",
      //   gender: "literature",
      //   rating: 5,
      //   image: "./assets/literature/3.jpg"
      // },
      // {
      //   name: "Hamlet",
      //   price: "27.28",
      //   gender: "literature",
      //   rating: 5,
      //   image: "./assets/literature/4.jpg"
      // },
      // {
      //   name: "A tragédia de Macbeth",
      //   price: "20.00",
      //   gender: "literature",
      //   rating: 4.4,
      //   image: "./assets/literature/5.jpg"
      // },
      // {
      //   name: "A cinco passos de você",
      //   price: "19.89",
      //   gender: "romance",
      //   rating: 4.5,
      //   image: "./assets/new/1.jpg"
      // },
      // {
      //   name: "A metamorfose",
      //   price: "23.68",
      //   gender: "fiction",
      //   rating: 4.3,
      //   image: "./assets/new/2.jpg"
      // },
      // {
      //   name: "A rainha vermelha",
      //   price: "27.99",
      //   gender: "fiction",
      //   rating: 4.4,
      //   image: "./assets/new/3.jpg"
      // },
      // {
      //   name: "A arte da guerra",
      //   price: "9.90",
      //   gender: "leadership",
      //   rating: 4.2,
      //   image: "./assets/new/4.jpg"
      // },
      // {
      //   name: "Patrulha canina - 365 atividades e desenhos para colorir",
      //   price: "7.90",
      //   gender: "children",
      //   rating: 4.4,
      //   image: "./assets/new/5.jpg"
      // },
    ];
  }

}
